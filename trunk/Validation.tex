%!TEX root=LearningGoalConflicts.tex
\section{Validation}
\label{sec:validation}
In this section we evaluate our genetic algorithm with the aim of answering the following research questions:

\begin{itemize}
%\itemsep-0.2em
\item[RQ1] \emph{How effective and efficient is our approach to identify boundary conditions in requirement specifications?}
\item[RQ2] \emph{Is our approach able to identify boundary conditions that cannot be derived by related techniques?}
%\item[RQ3] \emph{Is it possible to use boundary conditions for diagnosing unrealisable LTL specifications?}
%\item[RQ4] \emph{Can our approach efficiently analyse reasonable big specifications?}
\end{itemize}

In order to answer RQ1, we consider various requirement specifications taken from the literature and different benchmarks, that feature both safety and liveness goals, and evaluate our genetic algorithm for identifying boundary conditions. In Section~\ref{sec:case-studies} we present the experimental evaluation on several case studies taken from \cite{Degiovanni+2016,vanLamsweerde+1998,Alur+2013}, previously used for assessing a related technique for computing boundary conditions based on a tableaux satisfiability checking algorithm. In addition, we take several case studies used in the Reactive Synthesis Competition (SYNTCOMP)~\cite{SYNTCOMP}, publicly available at~\cite{SyntcompRepository}, which are considerable larger specifications than those taken from the literature that we mentioned before. These larger specifications will be used for assessing the scalability of our genetic algorithm.
%[TODO: Agregar alguna conclucion referente a esto]

To answer RQ2, in Section~\ref{sec:comparison-related-techniques} we briefly introduce two previously developed techniques for identifying boundary conditions, namely, a pattern-based approach~\cite{vanLamsweerde+1998} and a tableaux-based approach~\cite{Degiovanni+2016}, and compare the boundary conditions computed by our genetic algorithm against those obtained by the previous techniques. 
%[TODO: Agregar alguna conclucion referente a esto]

We analyse the obtained results in Sections~\ref{sec:sensitivity-discussion} and \ref{sec:applicability-discussion}, and discuss the scalability and applicability of our genetic algorithm. In particular, we argue about the different options that one can configure to run the genetic algorithm and how they may affect its efficiency and effectiveness. We also discuss some contexts in which the computed boundary conditions can be used, in addition to the most common one, during the identify-assess-control cycle in the risk analysis of requirements specifications. In particular, we study the possibility of using boundary conditions for explaining the cause of unrealisable specifications, in the context of automated synthesis. 
%we focus on the unrealisable specifications we took from the SYNTCOMP Repository~\cite{SyntcompRepository}, and study the possibility of using the computed boundary conditions for explaining why those specifications are unrealisable. 

To perform the experimental evaluation, we implemented our genetic algorithm using the Java Genetic Algorithms Package Library (JGAP)~\cite{JPGAP}, and integrating the LTL2B\"uchi library~\cite{GiannakopoulouLerda2002} to parse LTL requirements specifications, and the LTL satisfiability checker Aalta~\cite{Vardi+2015}, to perform all the SAT checks required by the fitness function. The tool, the specifications for all case studies, and a description of how to reproduce the experiments can be found in the replication package\footnote{\texttt{\url{https://dc.exa.unrc.edu.ar/staff/rdegiovanni/ASE2018.html}}}. All the experiments were run on an Intel Core i7 3.2Ghz, with 16Gb of RAM, running GNU/Linux (Ubuntu 16.04). 
%\url{https://sites.google.com/site/learninggoalconflicts/}

\subsection{Case Studies}
\label{sec:case-studies}
We evaluate our genetic algorithm on the following case studies: the Rail Road Crossing System~\cite{Beer+2015}, the Mine Pump Controller~\cite{Kramer+1983}, an elevator controller~\cite{Dwyer+1999}, the ATM~\cite{Uchitel+2003}, the TCP network protocol, the London Ambulance Service (LAS)~\cite{FinkelsteinDowell1996}, the Telephone~\cite{Felty+2003}, and the three patterns for deriving boundary conditions presented in~\cite{vanLamsweerde+1998} (Achieve, Retraction1, and Retraction2). Moreover, we consider the specification of a lift controller taken from~\cite{Alur+2013}, and some specifications of the SYNTCOMP Repository~\cite{SyntcompRepository}, namely, three variants of the arbiter synchronization protocol (simple, prioritized and round-robin), the ARM's Advanced Microcontroller Bus Architecture (AMBA), and a load balancer protocol for mutual exclusion.

Table~\ref{tab:effiency-evaluation} summarises the results of the experimental evaluation of our genetic algorithm. First, we report the size of the specification, i.e., the number of goals and domain properties, and the size of the initial population (I.P.) generated from such a specification. For each case study, we ran the algorithm 10 times, with a limit of 50 generations, i.e., 50 evolutions of the genetic algorithm population. Notice that we distinguish between the number of runs in which the genetic algorithm succeeded by identifying at least one boundary condition, and the number of runs in which the algorithm did not identify any boundary condition. For all of the successful runs, we report the minimum, maximum and average number of generations, and the corresponding time in seconds, required for learning the boundary condition. In particular, we focus on the cost of computing the first solution (the number of generations and time -- in seconds -- required to get a suitable boundary condition), and the cost of computing the ``best'' solution: notice that the algorithm continues running for 50 iterations, trying to optimise the boundary conditions collected so far, e.g., by making them more compact. 

\begin{table*}[htp!]
\caption{Evaluation of our Genetic Algorithm for Identifying Boundary Conditions}
\begin{center} 
%\scalebox{.65}{
\resizebox{\textwidth}{!}{
\begin{tabular}{|c|c|c|c||c|cc|cc|cc|cc|cc|cc||c|c|}
\hline
\multicolumn{4}{|c||}{}& \multicolumn{13}{c||}{BCs successfully found}& \multicolumn{2}{c|}{No BC found}\\
\multicolumn{4}{|c||}{Case Study}&\multicolumn{1}{c}{}&\multicolumn{6}{c}{First Solution}& \multicolumn{6}{c||}{Best Solution}& \multicolumn{2}{c|}{--}\\
\multicolumn{4}{|c||}{} & 
& \multicolumn{2}{c|}{\textbf{min}}& \multicolumn{2}{c|}{\textbf{max}}& \multicolumn{2}{c|}{\textbf{avg}}& \multicolumn{2}{c|}{\textbf{min}}& \multicolumn{2}{c|}{\textbf{max}}& \multicolumn{2}{c||}{\textbf{avg}}& \multicolumn{2}{c|}{--}\\
\hline
Spec. Name & \#Dom & \#Goals & I.P. & 
	\#Runs & Gen & time & Gen & time & Gen & time & Gen & time & Gen & time & Gen & time & 
	\#Runs & avg time\\
\hline
% \hline
%\multicolumn{18}{|c|}{Parameters: POPULATION SIZE = 100 \ CHROMOSOMES SIZE = 20 \ GENERATIONS = 50} \\
\hline																		
RRCS & 2 & 2 & 40 		& 8 & 7 & 2 & 11 & 4 & 9 & 2 & 9 & 2 & 50 & 20 & 36 & 17 & 2 & 28\\ \hline
MinePump & 1 & 2 & 34	& 10 & 3 & 1 & 17 & 12 & 6 & 2 & 3 & 1 & 38 & 24 & 18 & 7 & 0 & 0\\ \hline
ATM & 1 & 2 & 32		& 9 & 1 & 0 & 18 & 8 & 6 & 2 & 2 & 0 & 48 & 16 & 20 & 7 & 1 & 27\\ \hline
Elevator & 1 & 1 & 22 	& 10 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 0 & 0\\ \hline
TCP & 0 & 2 & 24		& 9 & 5 & 1 & 39 & 9 & 13 & 3 & 14 & 4 & 50 & 18 & 30 & 10 & 1 & 3\\ \hline
Telephone & 3 & 2 & 42 	& 3 & 4 & 4 & 28 & 29 & 14 & 14 & 21 & 29 & 78 & 86 & 30 & 53 & 7 & 44\\ \hline
LAS & 0 & 5 & 52		& 3 & 18 & 599 & 37 & 2355 & 26 & 1551 & 38 & 3431 & 47 & 18403 & 44 & 8491 & 7 & 4202\\ \hline
AchieveAvoidPattern & 1 & 2 & 32	& 10 & 1 & 0 & 8 & 2 & 4 & 1 & 3 & 0 & 26 & 11 & 13 & 5 & 0 & 0\\ \hline
RetractionPattern1 & 0 & 2 & 18 	& 10 & 4 & 1 & 38 & 14 & 12 & 4 & 5 & 1 & 50 & 29 & 25 & 17 & 0 & 0\\ \hline
RetractionPattern2 & 0 & 2 & 22 	& 8 & 1 & 0 & 25 & 9 & 7 & 2 & 6 & 1 & 39 & 28 & 24 & 16 & 2 & 38\\ \hline
Round Robin Arbiter & 6 & 3 & 94 	& 9 & 11 & 32 & 45 & 71 & 22 & 83 & 26 & 32 & 46 & 275 & 38 & 152 & 1 & 170\\ \hline
Simple Arbiter & 4 & 3 & 128		& 8 & 11 & 245 & 43 & 386 & 28 & 383 & 11 & 245 & 43 & 386 & 29 & 406 & 2 & 1007\\ \hline
Prioritized Arbiter & 6 & 1 & 84	& 4 & 15 & 257 & 42 & 523 & 30 & 7428 & 31 & 512 & 50 & 33687 & 43 & 8770 & 6 & 1582\\ \hline
Load Balancer & 3 & 8 & 102			& 3 & 15 & 185 & 44 & 9215 & 34 & 5253 & 44 & 6359 & 48 & 11262 & 46 & 6578 & 7 & 12595\\ \hline
%\hline
%\multicolumn{18}{|c|}{Parameters: POPULATION SIZE = 100 \ CHROMOSOMES SIZE = 50 \ GENERATIONS = 50}\\ 
%\hline
AMBA  & 6 & 21 & 362				& 6 & 24 & 3162 & 43 & 16342 & 30 & 7100 & 26 & 3162 & 49 & 7128 & 33 & 7541 & 4 & 11216\\ \hline
LiftController & 7 & 12 & 160		& 5 & 18 & 765 & 47 & 9690 & 34 & 2716 & 18 & 2126 & 47 & 9690 & 34 & 2853 & 5 & 22397\\ \hline
%Simple Arbiter & 4 & 3 & 8 & 11 & 245 & 43 & 386 & 28 & 383 & 11 & 245 & 43 & 386 & 29 & 406 & 2 & 1007\\ \hline
%Prioritized Arbiter & 6 & 1 & 10 & Running 203 &  &  &  &  &  &  &  &  &  &  &  &  & \\ \hline
% \hline
%\multicolumn{18}{|c|}{Parameters: POPULATION SIZE = 200 \ CHROMOSOMES SIZE = 50 \ GENERATIONS = 50}\\ 
%\hline
%LAS & 0 & 5 & 3 & 18 & 599 & 37 & 2355 & 26 & 1551 & 38 & 3431 & 47 & 18403 & 44 & 8491 & 7 & \\ \hline
%Load Balancer  & 3 & 8 & 10 & Running 206 &  &  &  &  &  &  &  &  &  &  &  &  & \\ \hline
%\hline
%\multicolumn{18}{|c|}{Parameters: POPULATION SIZE = 500 \ CHROMOSOMES SIZE = 50 \ GENERATIONS = 50 }\\ \hline
%Telephone & 3 & 2 & 3 & 4 & 4 & 28 & 29 & 14 & 14 & 21 & 29 & 78 & 86 & 30 & 53 & 7 & 43,85\\ \hline
\end{tabular}
}
\end{center}
\label{tab:effiency-evaluation}
\end{table*} 

% \begin{table*}[htp!]
% \caption{Evaluation of our Genetic Algorithm for Identifying Boundary Conditions}
% \begin{center} 
% %\scalebox{.65}{
% \resizebox{\textwidth}{!}{
% \begin{tabular}{|c|c|c||c|cc|cc|cc|cc|cc|cc||c|c|}
% \hline
% \multicolumn{3}{|c||}{}& \multicolumn{13}{c||}{BCs successfully found}& \multicolumn{2}{c|}{No BC found}\\
% \multicolumn{3}{|c||}{Case Study}&\multicolumn{1}{c}{}&\multicolumn{6}{c}{First Solution}& \multicolumn{6}{c||}{Best Solution}& \multicolumn{2}{c|}{--}\\
% \multicolumn{3}{|c||}{} & 
% & \multicolumn{2}{c|}{\textbf{min}}& \multicolumn{2}{c|}{\textbf{max}}& \multicolumn{2}{c|}{\textbf{avg}}& \multicolumn{2}{c|}{\textbf{min}}& \multicolumn{2}{c|}{\textbf{max}}& \multicolumn{2}{c||}{\textbf{avg}}& \multicolumn{2}{c|}{--}\\
% \hline
% Spec. Name & \#Dom & \#Goals & 
% 	\#Runs & Gen & time & Gen & time & Gen & time & Gen & time & Gen & time & Gen & time & 
% 	\#Runs & avg time\\
% \hline
% \hline																		
% RRCS & 2 & 2 & 8 & 7 & 2 & 11 & 4 & 9 & 2 & 9 & 2 & 50 & 20 & 36 & 17 & 2 & 28\\ \hline
% MinePump & 1 & 2 & 10 & 3 & 1 & 17 & 12 & 6 & 2 & 3 & 1 & 38 & 24 & 18 & 7 & 0 & 0\\ \hline
% ATM & 1 & 2 & 9 & 1 & 0 & 18 & 8 & 6 & 2 & 2 & 0 & 48 & 16 & 20 & 7 & 1 & 27\\ \hline
% Elevator & 1 & 1 & 10 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 0 & 0\\ \hline
% TCP & 0 & 2 & 9 & 5 & 1 & 39 & 9 & 13 & 3 & 14 & 4 & 50 & 18 & 30 & 10 & 1 & 3\\ \hline
% Telephone & 3 & 2 & 3 & 4 & 4 & 28 & 29 & 14 & 14 & 21 & 29 & 78 & 86 & 30 & 53 & 7 & 44\\ \hline
% LAS & 0 & 5 & 3 & 18 & 599 & 37 & 2355 & 26 & 1551 & 38 & 3431 & 47 & 18403 & 44 & 8491 & 7 & 4202\\ \hline
% AchieveAvoidPattern & 1 & 2 & 10 & 1 & 0 & 8 & 2 & 4 & 1 & 3 & 0 & 26 & 11 & 13 & 5 & 0 & 0\\ \hline
% RetractionPattern1 & 0 & 2 & 10 & 4 & 1 & 38 & 14 & 12 & 4 & 5 & 1 & 50 & 29 & 25 & 17 & 0 & 0\\ \hline
% RetractionPattern2 & 0 & 2 & 8 & 1 & 0 & 25 & 9 & 7 & 2 & 6 & 1 & 39 & 28 & 24 & 16 & 2 & 38\\ \hline
% Round Robin Arbiter & 6 & 3 & 9 & 11 & 32 & 45 & 71 & 22 & 83 & 26 & 32 & 46 & 275 & 38 & 152 & 1 & 170\\ \hline
% Simple Arbiter & 4 & 3 & 8 & 11 & 245 & 43 & 386 & 28 & 383 & 11 & 245 & 43 & 386 & 29 & 406 & 2 & 1007\\ \hline
% Prioritized Arbiter & 6 & 1 & 4 & 15 & 257 & 42 & 523 & 30 & 7428 & 31 & 512 & 50 & 33687 & 43 & 8770 & 6 & 1582\\ \hline
% Load Balancer & 3 & 8 & 3 & 15 & 185 & 44 & 9215 & 34 & 5253 & 44 & 6359 & 48 & 11262 & 46 & 6578 & 7 & 12595\\ \hline
% AMBA  & 6 & 21 & 6 & 24 & 3162 & 43 & 16342 & 30 & 7100 & 26 & 3162 & 49 & 7128 & 33 & 7541 & 4 & 11216\\ \hline
% LiftController & 7 & 12 & 5 & 18 & 765 & 47 & 9690 & 34 & 2716 & 18 & 2126 & 47 & 9690 & 34 & 2853 & 5 & 22397\\ \hline
% \end{tabular}
% }
% \end{center}
% \label{tab:effiency-evaluation}
% \end{table*} 

As our experiments show, our genetic algorithm can effectively compute boundary conditions for all of the case studies we considered. In the case of small specifications, like the RRCC, Minepump, etc., it can be very efficient, finding the first solution (i.e., a boundary condition) in a few generations. Of course, as the specification becomes more complex, the genetic algorithm needs more iterations to build richer formulas that lead us to boundary conditions, and consequently, it requires much more time. For instance, the worst case we reported is for computing the best solution for the Prioritized Arbiter protocol: our genetic algorithm required 33687 seconds, i.e., more than 9 hours. However, in average the performance of the algorithm is acceptable, considering that it can handle specifications with tens of formulas, that no other related technique can analyse.


\subsection{Comparison with Related Techniques}
\label{sec:comparison-related-techniques}

To answer RQ2, we now compare our approach with two related techniques for computing goal conflicts. The first one is a formal approach~\cite{vanLamsweerde+1998} that requires matching goals against a set of pre-defined divergence patterns, for which boundary conditions are provided. It provides three different patterns, namely, the Achieve-Avoid pattern, and two versions of the Retraction pattern. The first difference arises from the number of boundary conditions provided by each technique. Table~\ref{tab:comparison-tableau} summarises the number of boundary conditions learnt by our genetic algorithm. While patterns are designed to provide only one boundary condition per each divergence pattern, notice that our approach identifies several boundary conditions, that evidence multiple divergence situations that are not contemplated by the patterns. However, it is important to mention that in the case of the Achieve-Avoid and Retraction2 patterns, one boundary condition computed by our approach is equivalent to that provided by the patterns. On the other hand, despite the fact that our approach computes a large number of boundary conditions for the Retraction1 pattern, none of these is comparable (in terms of implication, i.e., either implied by or implying) with that provided by this pattern. This indicates that both techniques complement each other. In fact, one should always apply the patterns when possible, as these provide readable valid boundary conditions, and search for further boundary conditions with other techniques. 

The second technique that we consider in the comparison is an automated approach to compute boundary conditions, introduced in~\cite{Degiovanni+2016}. This technique performs a complex logical manipulation of the specification, by using a tableaux-based satisfiability checking algorithm, to identify boundary conditions. It applies to safety and liveness goals, as long as they can be expressed as reachability or response patterns \cite{MannaPnueli1992}. Table~\ref{tab:comparison-tableau} summarises the experimental results of applying this tool to the case studies considered here. The most notable difference is that the tableaux-based technique is not able to analyse all the specifications that our genetic algorithm supports. Basically, the generation of the tableau structure is very expensive, which becomes more noticeable in specifications containing various LTL formulas (in our case studies with larger sets of formulas, most are liveness properties). In these cases, the tableaux-based technique exceeded the timeout, set to 3 hours. The second difference resides again in the number of boundary conditions identified by each approach. While the tableaux-based technique can compute multiple boundary conditions, our genetic algorithm has consistently provided a larger set of divergence situations, that might not have been identified before. As it can be noticed in Table~\ref{tab:comparison-tableau}, some of the boundary conditions identified by the tableaux-based technique are implied ($\rightarrow$) by some one computed by our genetic algorithm. Others result to be equivalent to some boundary condition computed by our approach. And others resulted to be incomparable, evidencing different kinds of divergences identified by the two approaches. This, again, indicates that the tableaux-based technique can be used, as far as it is able to scale, for complementing the boundary conditions learnt with our approach. The last difference we highlight here is that the tableaux-based approach only applies to safety and liveness goals expressed with the reachability or response patterns, and computes boundary conditions with the general shape $\F \varphi$. In contrast, our genetic algorithm does not impose any restriction on the LTL formulation of the domain properties and goals, and it does not restrict the shape of the boundary conditions learnt to any particular pattern.   

%Solo se banca algunos patrones

\begin{table*}[htp!]
\caption{Comparison between our Genetic Algorithm and the Tableaux-based technique}
\begin{center} 
%\scalebox{.65}{
\resizebox{.8\textwidth}{!}{
\begin{tabular}{|c||c|c|c|c|c|c||c|c|c|c|c|c|}
\hline
& \multicolumn{6}{c||}{Genetic Approach - \#BCs} & \multicolumn{2}{c|}{Tableaux Approach } & \multicolumn{4}{c|}{Relation}\\
Case Study 	& 	\textbf{min} & time & \textbf{max} & time & \textbf{avg} & time & \#BCs &	time & $\rightarrow$ & $\leftarrow$ & $\equiv$ & $\not \equiv$\\
\hline
RRCS & 5 & 14 & 21 & 23 & 16 & 22 & 1 & 1 & 1 & 0 & 0 & 0\\ \hline
MinePump & 5 & 4 & 53 & 18 & 18 & 9 & 2 & 9 & 0 & 0 & 1 & 1\\ \hline
ATM & 4 & 9 & 20 & 17 & 10 & 10 & 4 & 0 & 0 & 0 & 0 & 4\\ \hline
Elevator & 3 & 3 & 17 & 5 & 7 & 3 & 1 & 0 & 1 & 0 & 0 & 0\\ \hline
TCP & 4 & 16 & 12 & 20 & 8 & 15 & 2 & 1 & 0 & 0 & 0 & 2\\ \hline
Telephone & 9 & 40 & 48 & 86 & 24 & 66 & 1 & 5 & 0 & 0 & 0 & 1\\ \hline
LAS & 46 & 3729 & 129 & 20370 & 84 & 9349 & 1 & 5 & 1 & 0 & 0 & 0\\ \hline
AchieveAvoidPattern & 12 & 8 & 38 & 11 & 21 & 10 & 4 & 2 & 2 & 0 & 0 & 2\\ \hline
RetractionPattern1 & 18 & 30 & 39 & 42 & 27 & 39 & 1 & 0 & 1 & 0 & 0 & 0\\ \hline
RetractionPattern2 & 11 & 14 & 38 & 44 & 22 & 36 & 1 & 0 & 0 & 0 & 1 & 0\\ \hline
Round Robin Arbiter  & 4 & 41 & 103 & 226 & 37 & 174 & \multicolumn{6}{c|}{TIMEOUT} \\ \hline
Simple Arbiter  & 1 & 165 & 44 & 892 & 15 & 1704 & \multicolumn{6}{c|}{TIMEOUT} \\ \hline
Prioritized Arbiter & 9 & 687 & 19 & 33751 & 13 & 8893 & \multicolumn{6}{c|}{TIMEOUT} \\ \hline
Load Balancer & 2 & 6885 & 4 & 13410 & 3 & 7565 & \multicolumn{6}{c|}{TIMEOUT} \\ \hline
AMBA & 1 & 38999 & 7 & 12286 & 2 & 13404 & \multicolumn{6}{c|}{TIMEOUT} \\ \hline
LiftController & 1 & 9302 & 7 & 14963 & 3 & 15531 & \multicolumn{6}{c|}{TIMEOUT} \\ \hline
\end{tabular}
}
\end{center}
\label{tab:comparison-tableau}
\end{table*} 

\subsection{Scalability and Sensitivity}
\label{sec:sensitivity-discussion}

Basically, a genetic algorithm is a search-based algorithm that is guided to solutions by a fitness function. There are many parameters that may considerably affect the performance of a genetic algorithm: the maximum number of iterations (generations), the size of the population, the maximum size for the chromosomes (in our case that chromosomes have varying lengths), the probability with which certain genetic operator is applied (mutation and crossover rates), etc. An incorrect setting of these parameters may not only affect the performance of the algorithm, it may also affect its effectiveness. 

In our algorithm, the maximum number of genes per chromosome is a parameter to which the algorithm is very sensitive. If we select a relatively small size for the chromosomes, the algorithm may be limited to finding boundary conditions that would otherwise be discovered with larger chromosomes, or not discovered at all, if not expressible with such small number of genes. So, this parameter needs to be adapted depending on the specifications involved on each case study. In the case of small specifications, like the MinePump and ATM, 20 genes per chromosome proved to be enough for identifying various boundary conditions. But in the case of larger specifications, as the AMBA or the LiftController case studies, the genetic algorithm required larger chromosomes (e.g., 50 genes per chromosome) to characterise the complex LTL properties involved in the kind of specifications used to express the domain properties and goals.

In order to assess the sensitivity of the genetic algorithm to other parameters, we studied how the effectiveness and efficiency of the algorithm is affected by the progressive variation of these, as it is customary in the context of genetic algorithms. We focused on two parameters, namely, the size of the population maintained per iteration, and the probability with which the genetic operators are applied. On one hand, as it was expected, as the size of the population is increased, we notice that the effectiveness of our genetic algorithm is increased as well. Of course, the efficiency is affected too, since the algorithm has more candidate solutions to which apply the genetic operators and evaluate the fitness function. On the other hand, we observed that the effectiveness and the efficiency of the algorithm do not seem to be affected by the rate of the crossover operator. It is not the same for the mutation operator; as we vary the mutation rate, the effectiveness of the algorithm is affected, but not its efficiency. For instance, in the Telephone case study, with a mutation rate set in 10\%, the algorithm was able to find boundary conditions in 5 runs out of 10, i.e., it had an effectiveness of 50\%, a 20\% more than that reported in Table~\ref{tab:effiency-evaluation}. Thus, despite the fact that our case studies show that our genetic algorithm scales to specifications that cannot be handled by related approaches, we believe that the performance of the algorithm can still be significantly improved by appropriate parameter setting. A more exhaustive experimental evaluation is required to try to identify different classes of problems, and establish well suited configurations of our genetic algorithm in these classes.
%Notice for instance, that the tableaux-based technique is considerably faster than our genetic algorithm for analysing the LAS case study. It give us the guide that we can select a better configuration than the current one, at least to improve the performance on that case study.

\subsection{Applicability and Usability}
\label{sec:applicability-discussion}

Goal-conflict analysis is typically driven by the identify-assess-control cycle, aimed at identifying, assessing and resolving conflicts that may obstruct the satisfaction of the goals. In particular, the assessment step is concerned with evaluating how likely the identified conflicts are, and how likely and severe are their consequences. The identified conflicts whose likelihood deems them critical, have to be resolved by providing appropriate countermeasures. Notice that for some of the case studies, e.g., the LAS and the round robin arbiter, our genetic algorithm identifies more than one hundred boundary conditions in some runs. Situations like this may make the assessment and control steps very expensive, and even impractical. In order to provide the engineer with an acceptable number of conflicts to be analysed, once the genetic algorithm finished, we perform a number of SAT checks, to attempt to reduce the set of boundary conditions to a smaller set of ``more general'' ones. Formally, if $BC_1$ implies $BC_2$, we say that $BC_2$ is more general, or weaker, than $BC_2$. This implication can be checked by using the LTL SAT solver: $BC_1 \land \neg BC_2$ is unsatisfiable when $BC_1$ implies $BC_2$. Table~\ref{tab:weaker-bcs} reports, for each case study, the number of more general BCs. Notice that the number of BCs to be analysed by the engineer can be considerably reduced. 

\begin{table}[htp!]
\caption{Number of weakest boundary conditions}
\begin{center} 
%\scalebox{.65}{
\resizebox{.4\textwidth}{!}{
\begin{tabular}{|c|c|c|}
\hline
Case Study & \#BCs & \#weakest BCs \\
\hline																
MinePump & 53 & 9\\ \hline
ATM & 20 & 5\\ \hline
Elevator & 17 & 3\\ \hline
RRCS & 21 & 3\\ \hline
TCP & 12 & 4\\ \hline
Telephone & 48 & 7\\ \hline
LAS & 129 & 8\\ \hline
AchievePattern & 38 & 9\\ \hline
RetractionPattern1 & 39 & 2\\ \hline
RetractionPattern2 & 38 & 1\\ \hline
Round Robin Arbiter Unreal 1 & 103 & 10\\ \hline
AMBA Unreal 1 & 7 & 6\\ \hline
LiftController & 7 & 5\\ \hline
Simple Arbiter Unreal 1 & 44 & 6\\ \hline
Load Balancer Unreal 1 & 4 & 2\\ \hline
Prioritized Arbiter Unreal 1 & 19 & 4\\ \hline
\end{tabular}
}
\end{center}
\label{tab:weaker-bcs}
\end{table} 

Let us now argue about the usefulness of the computed boundary conditions. Consider the MinePump example, the system that controls a pump in a mine, whose main goal is to avoid a flooding in the mine. The system can detect when the level of the water is high ($hw$) and when there is methane in the environment ($m$), since switching on the pump in the presence of methane may produce an explosion. The proposition $po$ is used to indicate that the pump is on. Assume now that the we would like to synthesise a controller that satisfies the following specification:

\vspace{5pt}
\noindent
\textbf{Domain}: \textit{PumpEffect}\\\
\textbf{InformalDef}: If the pump is on, the level of water decreases in at most two time units.\\
\textbf{FormalDef}: $\G (\G_{\leq 2} (po) \Implies \F_{\leq 2}(\neg hw))$

\vspace{5pt}
\noindent
\textbf{Goal}: \textit{NoExplosion}\\\
\textbf{InformalDef}: The pump should be off when methane is detected.\\ %in the mine.\\
\textbf{FormalDef}: $\G (m \Implies \X(\neg po))$
                                                             
\vspace{5pt}
\noindent
\textbf{Goal}: \textit{NoFlooding}\\
\textbf{InformalDef}: The pump should be on when the water level is above the high threshold.\\
\textbf{FormalDef}: $\G (hw \Implies \X(po))$

\vspace{5pt}

\noindent
One of the boundary conditions identified by our approach is $BC_1: \F(m \land hw)$, which coincides with the one manually identified in ~\cite{Letier2001phd}, and automatically discovered by the tableaux-based approach~\cite{Degiovanni+2016}. In order to get rid of this conflict, \cite{Letier2001phd} proposes to refine the goal \textit{NoFlooding}, by weakening it, requiring to switch on the pump when the level of water is high and no methane is present in the environment. Thus, \textit{NoFlooding'}: $\G (hw \land \mathbf{\neg m} \Implies \X(po))$. Despite the fact that this refinement removes the boundary condition $BC_1$, our genetic algorithm still computes 43 additional boundary conditions on the refined specification (3 of them are ``more general''), that would demand further attention and subsequent refinements. On the other hand, the tableaux-based approach does not identify any conflict in the refined specification.
%( X ( ! p  )  /\  h  /\  !m ) \/ (X (  m  /\  X ( p ) ))
%( <> ( (  h /\  ! m )  W (  m  /\  X ( p )  ) ) )
%( (!p  U ( h  /\ !  m )) W ( X ( m  /\ X (p) ) ) )

In addition to the mentioned use of boundary conditions, other application contexts may be explored, e.g., in synthesis settings. The problem of synthesis consists of automatically producing, from a given specification, an operational model, usually called the \emph{controller}, that by interacting with the environment in which it is executed, it allows it to satisfy a corresponding specification. 
%(e.g., an abstract model, like labelled transition systems, or a more concrete model, like a concrete program) 
LTL has been widely used as the specification language in the context of automated synthesis~\cite{MannaWolper1981,Klein+2012,DIppolito+2010}; both the environment and the properties to be satisfied by the controller are captured in many cases in terms of LTL assertions. %, namely, the domain properties and the goals, respectively. 
Typically, a synthesis tool has two possible outputs: that the specification is \emph{realisable}, and a controller is returned; or that the specification is \emph{unrealisable}, meaning that it is not possible to build a controller to guarantee the goals (i.e., the environment always has an strategy to violate them). Unfortunately, when the specification is not realisable, synthesis tools in general do not provide useful feedback to help the user understand why his/her specification is unrealisable. Boundary conditions can be used as declarative sentences useful for diagnosing unrealisable specifications. 

For instance, if we consider our previous specification for the Mine Pump Controller, and we ask some synthesis tool, like Ratsy~\cite{Bloem+2010}, if it is possible to build a controller that satisfies the specified goals, we will get as an answer that the specification is unrealisable. Recall that two boundary conditions computed for this specification were $BC_1: \F(m \land hw)$ and $BC_2 : \F(hw \land \neg m \land po \land \X (\neg hw \land \neg po \lor  hw \land (m \lor \neg po)))$. These formulas give us information of some admissible behaviours of the system, that lead us to violating the goals. 
%However, it could be possible that the controller has a winning strategy to avoid reaching the boundary condition and consequently, violates the goals.
So, if the controller is able to avoid reaching a boundary condition, then such condition is not the reason of the unrealisability of the specification. But, if the controller cannot avoid reaching the boundary condition, then it means that the environment always have a winning strategy to force the controller to reach the boundary condition. In that case, such a BC could be thought of as an explanation of why the controller cannot satisfy the goals, i.e., an explanation of unrealisability of the specification. Returning to the example, if we use Ratsy to check if it is possible to build a controller that avoids boundary condition $BC_2$ (i.e., we try to synthesize a controller for the goal $\neg BC_2$), then Ratsy will answer that such a goal is realisable. However, if we try to synthesise a controller for goal $\neg BC_1$, Ratsy will answer that such a goal is unrealisable, meaning that it is not possible for the controller to avoid reaching $BC_1$. Thus, $BC_1$ can be used to explain why the controller cannot guarantee both goals \textit{NoExplosion} and \textit{NoFlooding} at the same time. 

This is a promising application of boundary conditions as explanations of synthesis unrealisability, which needs to be further investigated, and opens a line of future work.
%Despite this being a promising application of boundary conditions to explain the unrealisability of specifications, we need to study the applicability on further examples, the adaptation of the generated explanations, and the particularities and limitations of different synthesis tools. This will require further investigation, and for us, open a line of future work.

%Usually, tools for LTL synthesis do not support any kind of goals; in general, they restrict to fragments of LTL, like GR(1)~\cite{Piterman2006}. On the other hand, our genetic algorithm, as is, does not any restriction on the boundary conditions to be computed. Then, for most of the case studies we could not use Ratsy in the way we used it on the MinePump controller. Because of that, this requires further investigation, and for us opens a line of future work.


