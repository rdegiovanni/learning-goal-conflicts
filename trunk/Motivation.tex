%!TEX root=LearningGoalConflicts.tex
\section{Motivation}
\label{sec:motivation}

In this section, we will illustrate through a running example both the problem we tackle in this paper, and the main ideas behind our approach based on a genetic algorithm. The example we will use is a simple rail road crossing system (RRCS)~\cite{Beer+2015}. In this model, a train may approach and enter a crossing; these events are captured by two propositions, $ta$ and $tc$, respectively. A car may also approach and enter the crossing, and these events are captured by $ca$ and $cc$, respectively. The crossing gate may be opened ($go$) or closed ($\neg go$). Whenever the train is approaching, the gate should be closed, and it will be reopened after the train has left the crossing. On the other hand, whenever a car approaches the crossing, it will be able to cross only if the gate is open. Let us assume that from the analysis of the above statements, the following goals and domain properties have been elicited:

\vspace{3pt}
\noindent
\textbf{Domain property}: \textit{TrainsDoNotStop}\\
%\textbf{InformalDef}: Trains do not stop at the crossing.\\
\textbf{FormalDef}: $ \G (\X(tc) \Iff ta)$

\vspace{3pt}
\noindent
\textbf{Domain property}: \textit{CarsCrossWhenGateIsOpened}\\
%\textbf{InformalDef}: Cars cross the crossing, only when the gate is opened.\\
\textbf{FormalDef}: $ \G (\X(cc) \Implies ca \land go)$

\vspace{3pt}
\noindent
\textbf{Goal}: Avoid[\textit{Collision}]\\
%\textbf{InformalDef}: Cars and trains shall not cross at the same time. \\
\textbf{FormalDef}: $\G \neg(tc \land  cc)$

\vspace{3pt}
\noindent
\textbf{Goal}: Maintain[\textit{ClosedGateWhenTrainApproaching}]\\\
%\textbf{InformalDef}: Whenever the train approaches, the gate shall be closed. \\
\textbf{FormalDef}: $\G (ta \Implies \neg go)$

\noindent 
Notice that the specification is \emph{consistent}, i.e., all goals can simultaneously be satisfied, for instance when no train and no car approach the crossing. However, this specification can exhibit some \emph{conflicts}, in particular when the controller opens the gate at the same time that the train is crossing (i.e., $go \land tc$), enabling an approaching car to cross as well, and consequently to collision with the train (i.e., $cc \land tc$). This conflicting situation can be characterised by a \emph{boundary condition}, in this case $(go \land tc)\ \W\ (cc \land tc)$. Boundary conditions generalise conflicting situations, by capturing similar contending scenarios by formulas in the same language used to express goals and domain properties. Both identifying conflicting situations and devising corresponding boundary conditions are non-trivial tasks (recall in particular the conditions that formulas must satisfy to be boundary conditions). In this work, we propose using a genetic algorithm to automatically discover boundary conditions from a formal goal model, with goals and domain properties expressed in LTL. The algorithm deals with a very large search space of LTL formulas, built through a syntactic manipulation of the formal domain properties and goals. Let us provide some intuition on how our approach works.

Intuitively, a chromosome in our genetic algorithm represents an LTL formula $\varphi$, in such a way that each gene of the chromosome characterises a sub-formula of $\varphi$. We consider as the initial population of our genetic algorithm the set of all sub-formulas that can be built from the domain properties and the goals, and their corresponding negations. For instance, from the goal \textit{ClosedGateWhenTrainApproaching} of our running example, the chromosomes that characterise the following 5 sub-formulas are created: $\G (ta \Implies \neg go)$, $ta \Implies \neg go$, $ta$, $\neg go$ and $go$; as well as their corresponding negations: $\neg \G (ta \Implies \neg go)$,  $\neg(ta \Implies \neg go)$ and $\neg ta$ (notice that, $go$ and $\neg go$ have already been considered).

In order to obtain new individuals to make the population evolve, some chromosomes are selected at each iteration, and some genetic operators are applied to these. In particular, our genetic algorithm implements the two most common genetic operators, namely, \emph{crossover} and \emph{mutation} operators. Given two chromosomes $c_1$ and $c_2$, the crossover operator will produce a new chromosome $c_3$ using parts of $c_1$ and $c_2$. For instance, if chromosomes $c_1$ and $c_2$ characterise the LTL formulas $go$ and $tc$, respectively, our crossover operator can produce a new chromosome by combining both formulas using some binary operator, such that $go \land tc$. On the other hand, given a chromosome $c_1$, the mutation operator will create a new chromosome $c_2$ by randomly changing some genes of $c_1$. For instance, if chromosome $c_1$ characterises the LTL formula $\G \neg (tc \land cc)$, a particular mutation can be performed, that removes the $\G$ operator, obtaining the formula $\neg (tc \land cc)$.

In a genetic algorithm, the population iteratively evolves guided by a \emph{fitness function}, whose aim is to evaluate individuals, giving higher scores to ``better'' individuals, i.e., those closer to sought for solutions. This has the aim of guiding the genetic algorithm to generating an individual that represents a solution to the problem being solved. In our case, the fitness function performs a number of SAT calls, to an LTL SAT solver, in order to check, given a chromosome, whether it meets all the conditions to be a boundary condition for the requirements specification or not. At the end of each iteration, those chromosomes with best fitness are selected to move to the next iteration.

Let us consider a specific example, to show how our genetic operators may lead to boundary conditions. Consider the boundary condition $(go \land tc)\ \W\ (cc \land tc)$ for the specification of the rail-road crossing system. The following trace of the genetic algorithm exemplifies how this particular formula may be generated:
\begin{enumerate}
\item Initialize the population with the set of all sub-formulas of the specification, and their negations. In particular, propositions $go$ and $tc$ will be characterised by some chromosomes $c_1$ and $c_2$, respectively.
\item Then, assume that our algorithm selects both chromosomes $c_1$ and $c_2$ to apply the crossover operator, producing a new chromosome $c_3$; in this particular combination, the $\land$ operator is employed, obtaining the LTL formula $(go \land tc)$.
\item Now assume that chromosome $c_1'$ characterising the goal $\G \neg (tc \land cc)$ (it will certainly be in the initial population), is selected for the application of a mutation, in particular one that acts by removing the temporal operator $\G$; we then obtain a new chromosome $c_2'$ that characterises formula $\neg (tc \land cc)$.
\item Now the algorithm selects chromosome $c_2'$ and applies a mutation operator similar to the previously mentioned, but this time it removes the $\neg$ logical operator, leading to chromosome $c_3'$, that characterises the formula $(tc \land cc)$.
\item Finally, the algorithm selects chromosomes $c_3$ and $c_3'$ to apply the crossover operator that combines them with the $\W$ operator, obtaining a new chromosome $c_4$ representing our target boundary condition: $(go \land tc)\ \W\ (cc \land tc)$.
\end{enumerate}

Of course, the above ``trace'' is one very specific, of the many that the genetic algorithm may involve. The same boundary condition may be produced with other different paths, and more importantly, many more paths will never produce the boundary condition. To guide it to our desired formulas, the fitness function plays a very important role (as well as the crossover and mutation operators in the definition of the search space), as well as other parameters of the algorithm, like the mutation and crossover rates, population size, individual selection approach, number of generations to consider, etc. The following section will provide further details on all these aspects of our genetic algorithm for boundary condition discovery. 

