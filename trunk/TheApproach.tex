%!TEX root=LearningGoalConflicts.tex
\section{A Genetic Algorithm for Identifying Boundary Conditions}
\label{sec:approach}

As we mentioned in previous sections, the objective of our genetic algorithm is to find situations that capture divergences in the LTL formulation of the requirements specification. In order to express such situations in the same formalism, the search space of our algorithm is composed of LTL formulas. The next sub-subsections detail the representation of the candidate LTL formulas as sequences of genes as well as the main components of the algorithm.

\subsection{Chromosome Representation} % of LTL Formulas}
\label{sec:ltltochrom}

To represent an LTL formula as a chromosome, i.e., a vector of genes, we first translate the formula to its definitional conjunctive normal form (dCNF)~\cite{Schuppan2012}. This gives us a simple and intuitive way of splitting LTL formulas into sub-formulas, in order to generate their corresponding chromosomes. 

Given an LTL formula $\varphi$, where $AP$ is the set of propositional variables used in $\varphi$, let $X = \lbrace x_0, x_1, \ldots \rbrace$ be a set of fresh propositional variables, such that, $AP \cap X = \emptyset$. Then, according to~\cite{Schuppan2012}, $dCNF_{aux}(\varphi)$ is a set of conjuncts, defined over $AP \cup X$, such that each conjunct represents of a sub-formula $\psi$ of $\varphi$. $dCNF_{aux}(\varphi)$ is defined inductively on the structure of the sub-formula $\psi$ as follows:
\begin{center}
\begin{tabular}{ ll} 
 \hline
 $\psi$ & Conjunct in $dCNF_{aux}(\varphi)$ \\
 \hline 
 $b$ with $b \in \mathbb{B}$ & $x_{\psi} \leftrightarrow b$\\ 
 $p$ with $p \in AP$ & $x_{\psi} \leftrightarrow p$  \\
 $o_{1}\psi'$ with $o_{1} \in \lbrace \neg, \X, \F, \G \rbrace$ & $x_{\psi} \leftrightarrow o_{1}x_{\psi'}$  \\
 $\psi'\ o_{2}\ \psi''$ with $o_{2} \in \lbrace \vee, \wedge, \U, \R, \W \rbrace$ & $x_{\psi} \leftrightarrow x_{\psi'}\ o_{2}\ x_{\psi''}$  \\ 
 \hline
\end{tabular}
\end{center}
Thus, the \textit{definitional conjunctive normal form} of $\varphi$ can be defined in the following way:
\begin{center}
$dCNF(\varphi) \equiv x_{\varphi} \wedge \G \bigwedge\limits_{c \in dCNF_{aux}(\varphi)} c$
\end{center}

Given the dCNF representation of $\varphi$, we can directly build the chromosome representing $\varphi$ as follows:
\begin{center}
 $ [  c_{1} , c_{2} , ... , c_{k} ]$ where each $c_{i} \in dCNF_{aux}(\varphi)$
\end{center}
Notice that, as opposed to what is common in genetic algorithms, these chromosomes have varying lengths, since different LTL formulas can have a different amount of conjuncts in their corresponding dCNF representations. 

\subsection{Initial Population}

Since the initial population is a sample of the search space, the routine used for generating it plays an important role. Taking advantage of the formulas present in the domain properties as well as the goals of the given specification, we define the set of formulas $S = Dom \cup G$ and calculate the set of sub-formulas $SF(\psi)$, for each $\psi \in S$, using the following recursive definition:
\begin{center}
\resizebox{0.48\textwidth}{!}{
\begin{tabular}{ l@{}l@{}l } 
$\psi = b$ or $p$ & with $b \in \mathbb{B}, p \in AP$ &: $SF(\psi) = \lbrace \psi \rbrace$ \\
$\psi = o_{1}\psi'$ & with $o_{1} \in \lbrace \neg, \X, \F, \G \rbrace$ &: $SF(\psi) = \lbrace \psi \rbrace \cup SF(\psi')$ \\
$\psi = \psi'o_{2}\psi'$ & with $o_{2} \in \lbrace \vee, \wedge, \U, \R, \W \rbrace$ &: $SF(\psi) = \lbrace \psi \rbrace \cup SF(\psi') \cup SF(\psi'')$ \\
\end{tabular}
}
\end{center}
Having the set $SF(S)$ of all sub-formulas of each $\psi \in S$, the initial population of individuals is defined to be the following set $IP$:
$$IP = SF(S) \cup \lbrace \neg s | s \in SF(S)\rbrace$$
Basically, the initial population is the set of all the sub-formulas, as well as their negations, that can be obtained from the domain properties and the goals of the specification. 

\subsection{Fitness Function}
\label{sec:fitness-function}
Since each chromosome in the population represents an LTL formula that is a candidate boundary condition for the current specification, the purpose of our fitness function is to evaluate how close is the formula to being an actual boundary condition. By using the definition in Section~\ref{sec:gore}, we can exactly determine when a formula is a boundary condition or not. Given a chromosome $c$ of length $l_{c}$ representing the LTL formula $\varphi_{c}$, the fitness value for $c$ is computed by the following function $f$:
\begin{center}
%\resizebox{0.48\textwidth}{!}{
$f(c) = li(\varphi_{c}) + \sum\limits_{i=1}^{|G|} min(\varphi_{c},G_{i}) + nt(\varphi_{c}) + \dfrac{1}{l_{c}}$
%}
\end{center}
where the functions $li$, $min$ and $nt$ are defined as follows:
\begin{center}
$li(\varphi_{c}) = \begin{cases} 1 &\mbox{if } \{Dom ,\varphi_{c},\bigwedge\limits_{1\leq i \leq n} G_i \} \models \False \\ 
0 & \mbox{ otherwise } \end{cases} $
\end{center}
\begin{center}
$min(\varphi_{c},G_{i}) =  \begin{cases} \dfrac{1}{|G|} &\mbox{if } \{ Dom, \varphi_{c}, \bigwedge\limits_{j \neq i} G_j \}\not \models \False \\ 0 & \mbox{ otherwise } \end{cases}$
\end{center}
\begin{center}
$nt(\varphi_{c}) =  \begin{cases} 0.5 &\mbox{if } \varphi_{c} \neq \neg (G_1 \land \ldots \land G_n)  \\ 0 & \mbox{ otherwise } \end{cases}$
\end{center}
Intuitively, the first three terms of the function $f$ capture the properties that a formula must satisfy in order to be a boundary condition, namely the logical inconsistency ($li$), the minimality ($min$) and the non-triviality ($nt$). With the aim of improving the readability of the produced boundary conditions, the last term of the function applies a penalty related to the formula length, that makes the genetic algorithm to tend to produce smaller formulas. Of course, this is a secondary issue, and this is why it only contributes a fraction to the fitness value, as opposed to the actual driving acceptance criterion, namely, the closeness of the formula to the satisfaction of the properties to be a valid boundary condition.

\subsection{Genetic Operators}

In order to explore the search space, our genetic algorithm implements a \textit{crossover operator} and a \textit{mutation operator}, both adapted to our chosen chromosome representation. 

Given two randomly selected chromosomes $c_{1}$ and $c_{2}$, representing the LTL formulas $\varphi_{1}$ and $\varphi_{2}$, respectively, our \emph{crossover operator} creates a new chromosome $c_{n}$, whose corresponding LTL formula $\varphi_{n}$ is calculated by applying one of the following operations:
\begin{enumerate}
\item $\varphi_{n} = \varphi_{1} o_{2} \varphi_{2}$, where $o_{2} \in \lbrace \vee, \wedge, \U, \R, \W \rbrace$;
\item $\varphi_{n} = \varphi_{1} [ s_{2} / s_{1} ]$, where $s_{1} \in SF(\varphi_{1})$ and $s_{2} \in SF(\varphi_{2}).$ 
\end{enumerate}   
Basically, case (1) corresponds to randomly taking a binary operator $o_{2} \in \lbrace \vee, \wedge, \U, \R\, \W \rbrace$, to create a new LTL formula $\varphi_1 o_2 \varphi_2$, in terms of formulas $\varphi_{1}$ and $\varphi_{2}$. On the other hand, case (2) corresponds to randomly taking a sub-formula $s_1$ of $\varphi_{1}$ and a sub-formula $s_2$ of $\varphi_{2}$, and creating a new formula $\varphi_n$, based on $\varphi_1$, but replacing its sub-formula $s_1$ by $s_2$ (i.e., $\varphi_{1} [ s_{2} / s_{1} ]$). Once the new LTL formula $\varphi_{n}$ has been built, the new chromosome $c_{n}$ is created, as described in Section~\ref{sec:ltltochrom}.

In contrast to the crossover operator, that applies at the chromosome level, the \emph{mutation operation} applies to randomly selected genes of a chromosome, i.e., it works at the gene level. Recall that each gene of a chromosome characterises a formula $\varphi$, that is a particular conjunct of the dCNF representation of $\varphi$. Then, genes have the generic form $x_{\psi} \leftrightarrow \psi$. In order to apply a mutation and maintain a valid dCNF representation, our mutation operation only alters the formula $\psi$ of the gene, obtaining a new formula $\psi'$. 
%Since the formula $\varphi$ can have different patterns (see \ref{sec:ltltochrom}), we implemented different mutations for each case. 
Let $g$ be a gene representing the conjunct $x_{\psi} \leftrightarrow \psi$. Our mutation operation is defined inductively on the shape of $\psi$, as follows:
\begin{itemize}
\item if $\psi = b$ or $\psi = p$, where $b \in \mathbb{B}$ and $p \in AP$, then:
\begin{enumerate}
	\item $\psi' = b'$, where $b' \in \mathbb{B}$
	\item $\psi' = r$, where $r \in AP$ and $r \neq p$
	\item $\psi' = \neg \psi$
\end{enumerate}
\item if $\psi = o_{1}x_{\psi_1}$, where $o_{1} \in \lbrace \neg, \X, \F, \G \rbrace$, then:
\begin{enumerate}
	\item $\psi' = x_{\psi_1}$ 
	\item $\psi' = o_{1}'x_{\psi_1}$ where $o_{1}' \in \lbrace \neg, \X, \F, \G \rbrace$ and $o_{1}' \neq o_{1}$
	\item $\psi' = \neg \psi$
\end{enumerate}
\item if $\psi = x_{\psi_1}o_{2}x_{\psi_2}$, where $o_{2} \in \lbrace \vee, \wedge, \U, \R, \W\rbrace$, then:
\begin{enumerate}
	\item $\psi' = x_{\psi_{r}}$ where $x_{\psi_{r}} \in \lbrace  x_{\psi_1}, x_{\psi_2} \rbrace$ 
	\item $\psi' = x_{\psi_1}o_{2}'x_{\psi_2}$ where $o_{2}' \in \lbrace \vee, \wedge, \U, \R, \W\rbrace$ and $o_{2}' \neq o_{2}$
	\item $\psi' = \neg \psi$
\end{enumerate}
\end{itemize}

\noindent
It is important to note that, for each case of $\psi$, all the mutations have the same probability to be chosen. In the case that $\psi$ is equal to a boolean value or a proposition, the possible mutations to be applied consist of replacing $\psi$ by \True, \False, or by its negation, or by a different proposition. In case that $\psi$ is a unary formula, then the possible mutations to be applied are the deletion of the unary operator from the formula, the replacement of the unary operator by a different one, or just the negation of $\psi$. Similarly, if $\psi$ is a binary formula, the possible mutations to be applied consist of removing the binary operator, and in this case, one of the operands should be chosen as the new $\psi'$, or replacing the binary operator by a different one, or just negating $\psi$.

Typically, how many crossovers are applied per generation, and with which probability a gene is mutated, are parameters that one is allowed to configure in a genetic algorithm, to improve effectiveness and efficiency. Our genetic algorithm considers the \%10 of the population size to be the number of times that the crossover operator is applied per generation. For instance, if the predefined population size is 100, then our genetic algorithm will perform 10 crossovers per generation. On the other hand, given a chromosome $c$, with $l_c$ being the number of genes of $c$, our genetic algorithm considers $1/l_c$ as the probability with which a gene will be mutated. This means that, in the long term, one gene per chromosome will be mutated. 

Finally, after applying the fitness function on each chromosome in the population, some of them should be selected to survive to the next generation. Our algorithm uses a very simple selection operator, based on sorting the individuals of the current population by its fitness values in decreasing order, and selecting as many individuals as the set maximum population size. We also experimented with the use of other known selection algorithms, such as the tournament selector, but in our case studies we always got better results with the ``best fitness'' selector. In the following section, we assess the performance of our algorithm in case studies of varying complexities, as well as its effectiveness in relation to other techniques.  


\subsection{Correctness and (In)completeness}

Let us now discuss the correctness and (in)completeness of our approach.
%MENTION IN SECTION 4 THAT THE ALGORITHM IS SOUND, MODULO SOUNDNESS OF LTL SAT CHECKING. 
Regarding correctness, our approach results to be \emph{correct}, i.e., if the genetic algorithm finds a formula that is solution of the problem, then this is indeed a valid boundary condition. Notice that, as it was explained in Section~\ref{sec:fitness-function}, the fitness function of the genetic algorithm performs, for each candidate solution, a number of SAT checks in order to determine if the candidate formula satisfies or not all the properties to be a boundary condition. Then, by relying on the correctness of the satisfiability solver for LTL used for this task, Aalta~\cite{Vardi+2015} in our case, our genetic algorithm is correct.

%REGARDING “COMPLETENESS”, MENTION THAT THE ALGORITHM IS NOT COMPLETE, SINCE GA IMPLEMENTS A NON-EXHAUSTIVE SEARCH, DESPITE THE FACT THAT THE GENETIC OPERATORS ARE COMPLETE, I.E., GIVEN ALPHA AND BETA FORMULAS OVER THE SAME VOCABULARY, BETA CAN BE PRODUCED THROUGH CROSSOVER/MUTATION FROM ALPHA PROVIDED A SUFFICIENTLY LARGE CHROMOSOME SIZE (THUS, ALL FORMULAS OVER THE SAME VOCABULARY CAN THEORETICALLY BE PRODUCED BY THE ALGORITHM).
Regarding completeness, since our genetic algorithm implements a \emph{non-exhaustive search}, our approach results to be \emph{incomplete}, i.e., there may exist some boundary conditions that are not be visited/considered by our genetic algorithm. However, it is important to remark that our genetic operators are complete, in the sense that, given two formulas $\varphi$ and $\psi$ over the same set $AP$ of propositions, $\psi$ can be produced from $\varphi$ by the application of crossover and mutation, provided a sufficiently large chromosome size. Thus, all formulas over the same vocabulary, that fit into the predefined chromosome size, can theoretically be produced by our genetic algorithm.


