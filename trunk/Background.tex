%!TEX root=LearningGoalConflicts.tex
\section{Background}
\label{sec:background}

\subsection{Goal-Oriented Requirements}
\label{sec:gore}
Goal-Oriented Requirements Engineering (GORE) \cite{VanLamsweerde2009} drives the requirements process in software development from the definition of high-level goals, that state how the system to be developed should behave. Goals are prescriptive statements that the system must achieve through the collaboration of cooperating agents, that might include humans, hardware devices and of course the software system, within a given domain. This domain must also be explicitly characterised, via \emph{descriptive} statements about the problem world, such as natural laws, collectively referred to as \emph{domain properties}. Within this context, a goal model consists of a decomposition of goals via refinements, capturing how a goal can be fulfilled in terms of simpler ones. 
Goal refinement terminates when every leaf subgoal can be assigned to a single agent, that will be in charge of guaranteeing its achievement (agents feature operations, through which they must fulfil the goals). %A \emph{requirement} is a terminal goal assigned to a software agent, while an \emph{assumption} is a goal assigned to an agent in the environment. 

Generally, the description of software requirements can be inadequate for various reasons. For instance, assuming an unrealistic benevolent behaviour of the environment can make the goals too ideal to be met. Also, some unanticipated cases can make a requirements specification incomplete. Even the goals themselves may be inconsistent as a whole, i.e., they may not be jointly satisfiable. 

In GORE methodologies, dealing with the above-cited kind of problems, as early as possible, is of most importance. The \emph{conflict analysis phase}~\cite{vanLamsweerdeLetier1998,VanLamsweerde2009} deals with these issues, through three main stages: \emph{(1)} the \emph{identification} stage, which consists of identifying conflicts between goals (i.e., conditions that, when present, make the goals inconsistent); \emph{(2)} the \emph{assessment} stage, consisting of assessing and prioritising the identified conflicts according to their likelihood and severity; and \emph{(3)}, the \emph{resolution} stage, where conflicts are resolved by providing appropriate countermeasures and, consequently, transforming the goal model, guided by the criticality level obtained during assessment. 

This paper focuses on the \emph{identification} stage, with the provision of an automated mechanism for goal conflict discovery. A \emph{conflict} essentially represents a condition whose occurrence results in the loss of satisfaction of the goals, i.e., that makes the goals \emph{diverge} \cite{vanLamsweerde+1998}. More formally, given a set $G_1,\ldots,G_n$ of goals and a set $Dom$ of domain properties, these are said to be \emph{divergent} if and only if there exists an expression $BC$, called a \emph{boundary condition}, such that the following conditions hold:
\begin{align*}
&\{Dom ,BC,\textstyle\bigwedge\limits_{1\leq i \leq n} G_i \} \models \False, \tag{\textit{logical inconsistency}} \\
&\{ Dom, BC, \textstyle\bigwedge\limits_{j \neq i} G_j \}\not \models \False, \textrm{for each } 1\leq i \leq n \tag{\textit{minimality}} \\
&BC  \neq \neg (G_1 \land \ldots \land G_n)   \tag{\textit{non-triviality}} 
\end{align*}

\noindent
Intuitively, a boundary condition captures a particular combination of circumstances in which the goals cannot be satisfied as a whole. The first condition establishes that, when $BC$ holds, the conjunction of goals $G_1,\ldots,G_n$ becomes inconsistent. The second condition states that, if any of the goals is disregarded, then consistency is recovered. The third condition prohibits a boundary condition to be simply the negation of the goals. Also, due to the minimality condition, it cannot be $\False$ (it has to be consistent with the domain $Dom$). Section~\ref{sec:motivation} provides an illustrating example, that further explains the intuition behind boundary conditions. 

Typically, formal requirements engineering methodologies adopt a logical formalism to precisely capture the desired system goals and domain properties. For instance, the KAOS method~\cite{VanLamsweerde2009} uses Linear-Time Temporal Logic~\cite{MannaPnueli1995} for formally specifying software requirements. Employing a formal language to specify software requirements enables the use of (semi-)automated analysis mechanisms, to assess specifications. For instance, if LTL formulas are used to specify requirements, one may use automated LTL satisfiability solvers to check for the feasibility of the corresponding requirements. As we will describe in detail later on, we will exploit efficient LTL satisfiability solvers to automatically check whether generated candidate formulas satisfy or not the conditions to be valid boundary conditions.

%As mentioned earlier, divergence is a form of weak conflict. \emph{Strong conflict}~\cite{vanLamsweerde+1998} is a particular case of divergence in which the boundary condition $\BC = \True$.
%In addition, the minimality condition can be useful for \emph{localising} the conflicting goals, making then easier to design the countermeasure to solve the conflicts.
%In this work we present an automated approach to compute boundary conditions for goals specified in Linear-Time Temporal Logic.


\subsection{Linear-Time Temporal Logic}
\label{sec:ltl}

Linear-Time Temporal Logic (LTL)~\cite{MannaPnueli1995} is a logical formalism that has been extensively employed to state properties of reactive systems, and more recently, for specifying software requirements~\cite{VanLamsweerde2009}. LTL assumes that the structure of time is linear, i.e., each instant of time is followed by a unique future instant. The syntax of LTL formulas is inductively defined using a set $AP$ of propositional variables, the standard logical connectives and temporal operators $\X$ and $\U$, as follows: \emph{(i)} $b \in \mathbb{B}$ is an LTL formula, where $\mathbb{B}=\{\True,\False\}$; \emph{(ii)} every proposition $p \in AP$ is an LTL formula, and \emph{(iii)} if $\varphi_1$ and $\varphi_2$ are LTL formulas, then so are $\neg \varphi_1$, $\varphi_1 \vee \varphi_2$, $\varphi_1 \wedge \varphi_2$, $\X \varphi_1$ and $\varphi_1 \U \varphi_2$. We consider the usual definition for the operators $\G$ (always), $\F$ (eventually), $\R$ (release) and $\W$ (weak-until) in terms of $\X$, $\U$, and logical connectives. 

LTL formulas are interpreted over infinite traces of propositional valuations. Let $\sigma$ be an infinite trace. Formulas with no temporal operators are evaluated in the first state of $\sigma$. On the other hand, $\X \varphi$ is true in $\sigma$ if and only if $\varphi$ is true in $\sigma[1..]$ (the trace obtained by removing the first state from $\sigma$), and $\varphi_1 \U \varphi_2$ is true in $\sigma$ if and only if there exists a position $i$ such that $\varphi_2$ holds in $\sigma[i..]$, and for all $0 \le j < i$, $\varphi_1$ holds in $\sigma[j..]$. 

The satisfiability problem for LTL consists of checking, given an LTL formula $\varphi$, if there exists at least one trace $\sigma$ that makes $\varphi$ hold, i.e., $\varphi$ evaluates to true in $\sigma$. LTL satisfiability is a \emph{decidable} problem \cite{DBLP:journals/sttt/RozierV10}, and there exist various tools that implement LTL satisfiability checking, so called LTL SAT solvers. As we explain later on in the paper, such tools are central to our evolutionary computation approach, since LTL SAT solving is employed as part of the fitness computation in the search for boundary conditions. 

We refer the reader to \cite{MannaPnueli1992} for further details on linear-time temporal logic. 
%Note by Naza: creo que esto no hace falta meterlo aca.
%In this work we focus on the LTL syntax to implement the mutation and crossover operators provided by the genetic algorithm.

\subsection{Genetic Algorithms}

Genetic algorithms \cite{Goldberg1989} are heuristic search algorithms, inspired in natural evolution. As opposed to more traditional search algorithms, that maintain a single ``current'' candidate solution during the search space traversal, a genetic algorithm operates on a \emph{population} of candidate solutions to a given problem. These candidates are called \emph{individuals} or \emph{chromosomes}, and are often represented as sequences of \emph{genes} (characteristics) that capture their features. A genetic algorithm starts with an initial population of individuals, whose individuals are produced in some arbitrary way, e.g., randomly, and explores the search space by iteratively \emph{evolving} the population, trying to generate a population containing an individual that represents a solution to the problem. At each iteration of this evolution process, members of the current population are selected to make the population evolve, by producing further individuals using two genetic operators: \emph{crossover}, that produces new individuals by combining parts of existing ones, and \emph{mutation}, that creates new individuals by randomly producing changes on existing ones. The selection of individuals to which the genetic operators will be applied, as well as the selection of individuals to be discarded after each iteration, are guided by a \emph{fitness function}. A fitness function is a heuristic function that measures how ``fit'' a particular individual is, i.e., how close a given candidate is to being an actual solution to the problem being solved. The evolution process is usually performed a defined number of iterations (known as \emph{generations} of the population), or until some termination criterion is met. 

As it will be described in later sections, we will employ genetic algorithms to search for boundary conditions of goal-oriented requirements specifications. Thus, individuals will in our case represent LTL formulas, the genetic operators will produce formulas from other formulas, and the fitness function should attempt to evaluate how ``close'' a formula is to being a boundary condition. 

For further details on genetic algorithms, we refer the reader to ~\cite{Michalewicz1996}. 



%Section~\ref{sec:approach} describes the details of our genetic algorithm for boundary conditions identification. 
